@echo off
echo Timer of shutdown
set /p a="The time that is through which computer must shut down in minutes: "
set /a b=%a*60
shutdown /s /t %b%
